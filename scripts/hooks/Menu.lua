UICustomizerMenu = UICustomizerMenu or class(BLTMenu)
function UICustomizerMenu:Init(root)
	self:Button({
		name = "ResetOptions",
		text = "ui_customizer_reset_all_opt",
		callback = ClassClbk(self, "ResetValuesFunc", true)
	})
	self:Button({
		name = "ResetOptions",
		text = "ui_customizer_reset_opt",
		callback = ClassClbk(self, "ResetValuesFunc", false)
	})
	self:Slider({
		name = "HUDScale",
		text = "ui_customizer_hudscale",
		min = 0.4,
		max = 1.5,
		default_value = UICustomizer.Options:GetOption("HUDScale").default_value,
		value = UICustomizer.Options:GetValue("HUDScale"),
		callback = callback(self, self, "MainCallback")
	})
	self:Slider({
		name = "HUDSpacing",
		text = "ui_customizer_hudspacing",
		min = 0.1,
		max = 1,
		default_value = UICustomizer.Options:GetOption("HUDSpacing").default_value,
		value = UICustomizer.Options:GetValue("HUDSpacing"),
		callback = callback(self, self, "MainCallback")
	})
	if managers.hud then
		managers.hud._saferect:panel():set_layer(-10000) --why is the hud not behind the menu AAA
		managers.hud:set_enabled()
		if managers.raid_menu._background_image then
			managers.raid_menu._background_image._panel:set_alpha(0.5)
		end
	end
end

function UICustomizerMenu:ResetValuesFunc(all)
	if all then
		UICustomizer.Options:SetValue("Tweak", {})
		UICustomizer.Options:Save()
		tweak_data:ApplyUICustomizer()
	end

	self:ResetValues()
	self:ReloadMenu()
end

function UICustomizerMenu:MainCallback(value, item)
	UICustomizer.Options:SetValue(item._params.name, value)
	if managers.hud then
		managers.hud:SetScale()
	end
end

function UICustomizerMenu:Close()
	if managers.hud then
		managers.hud._saferect:panel():set_layer(0)
		managers.hud:set_disabled()
		if managers.raid_menu._background_image then
			managers.raid_menu._background_image._panel:set_alpha(1)
		end
	end
end

UICustomizerColorTweakMenu = UICustomizerColorTweakMenu or class(BLTMenu)
function UICustomizerColorTweakMenu:Init(root, args)
	local reset = self:Button({
		name = "ResetOptions",
		text = "ui_customizer_reset_opt",
		callback = ClassClbk(self, "ResetValuesFunc")
	})
	local save_name = args.save_name
	local colors = {}
	for k, color in pairs(args.colors) do
		if k ~= "type_id" then
			local t = type(color)
			if t == "table" or t == "userdata" and color.type_name == "Color" then
				colors[k] = color
			end
		end
	end

	local panel = self:Panel({
		index = 2,
		y_offset = 20,
		w = root:w(),
		h = root:h() - reset:bottom() - 75,
	})

	local function create_customize(color, name, tbl, key, color_index)
		key = key or name
		self:ColorButton({
			text = string.pretty(name),
			value = color,
			parent = panel,
			localize = false,
			callback = function(newcolor)
				local tweak = UICustomizer.Options:GetValue("Tweak")
				tweak[save_name] = tweak[save_name] or {}
				if color_index then
					local copy_tbl = deep_clone(tbl[key])
					copy_tbl[color_index].color = newcolor
					tweak[save_name][key] = copy_tbl
				else
					tweak[save_name][key] = newcolor
				end
				UICustomizer.Options:Save()
				tweak_data:ApplyUICustomizer()
			end
		})
	end
	for k, v in pairs(colors) do
		if type_name(v) == "table" then
			for i, t in pairs(v) do
				if type(t) == "table" and t.color then
					create_customize(t.color, k .. " state #" .. i, colors, k, i)
				end
			end
		elseif type_name(v) == "Color" then
			create_customize(v, k, colors)
		end
	end
end

function UICustomizerColorTweakMenu:ResetValuesFunc()
	UICustomizer.Options:GetValue("Tweak")[self._save_name] = {}
	UICustomizer.Options:Save()
	tweak_data:ApplyUICustomizer()
	self:ReloadMenu()
end

Hooks:Add("MenuComponentManagerInitialize", "SomeMenu.MenuComponentManagerInitialize", function(self)
	RaidMenuHelper:CreateMenu({
		name = "ui_customizer",
		inject_menu = "blt_options",
		class = UICustomizerMenu
	})
	RaidMenuHelper:CreateMenu({
		name = "ui_customizer_colors_tweak_data",
		inject_menu = "ui_customizer",
		args = { colors = tweak_data.gui.colors, save_name = "TweakDataColors" },
		class = UICustomizerColorTweakMenu
	})
	RaidMenuHelper:CreateMenu({
		name = "ui_customizer_colors",
		inject_menu = "ui_customizer",
		args = { colors = Color, save_name = "MainColors" },
		class = UICustomizerColorTweakMenu
	})
end)
