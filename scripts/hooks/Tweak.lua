UICustomizer.default_colors = {}
function UICustomizer:ApplySettingsToTable(tbl, save_name)
    local tweak = UICustomizer.Options:GetValue("Tweak")[save_name]
    local def = UICustomizer.default_colors[save_name] or {}
    UICustomizer.default_colors[save_name] = def
    for k, v in pairs(tbl) do
        if tweak and tweak[k] then
            def[k] = def[k] or (type(v) == "table" and deep_clone(v) or v)
            tbl[k] = tweak[k]
        elseif def[k] then
            tbl[k] = def[k]
            def[k] = nil
        end
    end
end

function TweakData:ApplyUICustomizer()
    UICustomizer:ApplySettingsToTable(self.gui.colors, "TweakDataColors")
    UICustomizer:ApplySettingsToTable(Color, "MainColors")
end

tweak_data:ApplyUICustomizer()
